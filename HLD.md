# Sylva Terminology

Sylva is using three main components:

- the management cluster, which is intended to be deployed centrally and capable of spawning and managing Sylva workload clusters (aka downstream clusters), with a declarative approach;
- the workload cluster, which is the cluster that will host the CNF;
- the units that are applications (or components), mandatory or optional, coming from opensource projects that compose the Sylva clusters. Note that "units" is used here not to bring confusion with the Kustomize "components" terminology also used in Sylva.

# Sylva High Level Design

Depending on the infrastructure that will host the Sylva clusters, and the type of the clusters, the operator will use a subset of the units referenced by Sylva.
Units used for deploying rke2 clusters on baremetal servers will be different from the one used to deploy kubeadm clusters on Openstack virtual machines.

Considering the high number of clusters the Sylva stack will have to manage, a declarative approach will be used to manage both clusters and units. Gitops tools will be in charge of reconciling what is declared with what is deployed. It means that units kustomizations and cluster definitions have to be maintained in central repositories supervised by the Gitops tools.

In addition to the unit kustomizations maintained by the Sylva developers, each operator can maintain specific kustomizations to overload default values. It can be convenient to adapt the units to the corporate environment. Each operator can also easily add its own units to be deployed inside the clusters with the same Gitops workflow.

Based on the Gitops workflow, a dedicated team that wants to rely on the Sylva solution to deploy dedicated workload clusters, able to host targeted CNF, would have to specify the definition of those workload clusters in a git repository. This definition would then be taken into account by the gitops solution to deploy desired workload clusters.

The sylva-core project can be used to deploy a management cluster and some workload clusters, both containing the units that make sense for the targeted infrastructure environment.

![](./img/HLD.png)


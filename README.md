# Sylva Documents

If we want to better understand the context leading to the creation of the Sylva project, you can read the two following documents:

- the [white paper](White_Paper_Operators_Sylva.pdf)
- the [FAQ](FAQ_Sylva.pdf)
- the [technical charter](Project_Sylva_Technical_Charter.pdf)
- the [Technical Steering Committee](https://gitlab.com/sylva-projects/governance/-/wikis/home) 

# Sylva technical principles

This document introduces the main technical principles of the Sylva project. The main business drivers are detailed in a white paper.

# Sylva to answer to 5G and Edge use cases

Sylva main objective is to release a cloud native infrastructure stack to host Telco (5G, OpenRAN, CDN, etc.) and Edge use cases. The main technical challenges that Sylva is targeting are Network performance, Distributed Cloud (multi cluster Kubernetes, Bare Metal Automation), Energy Efficiency, Security (hardening and compliance) and Openness (capitalize and contribute to [Anuket](https://anuket.io/), [Nephio](https://nephio.org/), [O-RAN](https://www.o-ran.org/), etc.).
Sylva provides an environment to collaborate and develop solutions to tackle down specific technology challenges and facilitate cloud native adaption for Telco and Edge use cases.

# Five Technical Pillars

![Five Pillars](./img/fivepillars.png "Five Pillars")

## Network performance

Sylva project implements the requirements of performance starting with 5G Core and Open Ran Cloud native Network Functions (CNF). Concretely, those requirements are for instance SRIOV or Near-Real time OS. Those features are already described by [Anuket Reference Architecture for Kubernetes based cloud infrastructure (RA2)](https://cntt.readthedocs.io/projects/ra2/en/latest/index.html) and the O-RAN Cloudification and Orchestration Workgroup (WG6).

## Distributed Cloud

To address use cases such as distributed UPF in 5G Core, CDN or O-RAN, Sylva will provide an architecture able to manage cloud infrastructures from central locations to far edge sites.

> ![Illustration of sites distributions!](./img/distributedCloud.png "Illustration of sites distributions")
> Figure : Illustration of sites distributions

- **Declarative approach** is key to manage high volume of physical nodes and Kubernetes clusters. The principle is to describe the targeted infrastructure in a declarative way and use the Kubernetes reconciliation framework and controllers to manage the lifecycle of the infrastructure components based on the difference between the current and the declared state. The imperative mode with automation tool like Ansible is different as each step of build is described.
  The declarative model is a new step in the automation journey of the day 0 (build), day 1 (run) and day 2 (operate) management of networks, where the deployment process, fault management and upgrade are natively automated.
- **Kubernetes Clusters Life Cycle Management** Sylva needs to define a simple or robust way to manage  many Kubernetes clusters.
  Why are we facing a high volume of Kubernetes clusters ?
  
  - In a distributed cloud, an edge site (for example Radio Site) can face connectivity issues. In this context, a multi Kubernetes approach is more robust than a stretched Kubernetes cluster.
  - Due to the lack of hard multitenancy in Kuberentes some critical CNF (cloud native Network Functions) requires a dedicated Kubernetes cluster for a high security isolation (and possible also on isolated Hardware)
- **Hybrid deployment and Bare Metal Automation**
  In the distributed cloud, Sylva is integrating different deployment models to cover many use cases.  The appropriate deployment architecture will depend on the choice to use or not an existing cloud infrastructure (VMWare/OpenStack).High level of performance will be achieved with CaaS on Bare Metal.

> ![illustration of a hybrid deployment!](./img/hybridDeployment.png "illustration of a hybrid deployment")
> Figure : illustration of a hybrid deployment

## Security

Telco are increasingly exposed to cyber-attack (GSMA cyberthreat report 2021). So, states and EU regulations are tightening their expectations.
Following a cyberthreat analysis and using the actual version of the [EUCS](https://www.enisa.europa.eu/publications/eucs-cloud-service-scheme) requirements, Sylva project is continuously integrating security requirements.

> Some security domains covered :
> 
> - Hardening of all layers (OS, K8S)
> - Decreasing the surface of attack and isolation
> - Identity and Access Management
> - Internal & External cyphered communications
> - Security Logging, etc..

## Energy Efficiency

Sylva ambition is to address capabilities to manage a large volume of nodes, so from the starting point of the project energy control is integrated as a pillar.
In a first step we are integrating measure consumption with advanced analysis with Consumption per Microservice. The objective is to identify optimized or inefficient CNF in real condition.
In a second step Sylva will integrate optimization mechanism, for example based on kubernetes  scaling capabilities.

## Open source and standardized API

### Modular Design

The architecture choice of Sylva is modular, there are many open-source components that can be in the future replaced by alternative projects. The aim is to stay state of the art with the ability to integrate continuously new innovative components.

### Contribute to the Ecosystem

As presented in the figure below Sylva relies and contribute to existing telco ecosytems that provide requirements such as O-RAN alliance and  open-source projects.

To illustrate this, Sylva is inspired by the [Anuket Reference Implementation for Kubernetes based cloud infrastructure (RI2)](https://cntt.readthedocs.io/projects/ri2/en/latest/) and Sylva’s testing strategy is based on [Anuket Reference Conformance for Kubernetes based infrastructures (RC2)](https://cntt.readthedocs.io/projects/rc2/en/latest/index.html) testing framework. Following the Sylva implementation feedbacks, some enhancements will be then integrated directly to Anuket.

> ![Sylva and main Ecosystem interactions!](./img/ecosystem.png "Sylva and main Ecosystem interactions")
> Figure : Sylva and main Ecosystem interactions

#### Sylva support for O2 interface from O-RAN ALLIANCE

O-RAN ALLIANCE is currently in the process of specifying the O2 interface to enable the life cycle management of the O-RAN cloudified network functions, as well as the management of the cloud infrastructure itself. O2 interface consists therefore of two parts.  

The first one is the O2-DMS and it is dedicated to the LCM of Deployments (such as containerized NFs). Sylva project plans to support the O2-DMS natively because this interface is based on K8s.

The second one is the O2-IMS and it is dedicated to the LCM of the cloud infrastructure itself. Sylva aims to support this O2-IMS as well, but the extent of this support is currently under discussion among Sylva members.

#### Sylva support for ETSI NFV SOL018 and SOL020

ETSI NFV SOL018: Kubernetes® API is profiled to the requirements on the OS container management service interfaces exposed by the CISM as specified in ETSI GS NFV-IFA 040.

ETSI NFV SOL020: the Kubernetes® Cluster API is a Kubernetes® sub-project focused on providing declarative APIs and tooling to simplify provisioning, upgrading, and operating multiple Kubernetes® clusters. The Kubernetes® Cluster API (Cluster API, or CAPI in abbreviation) is profiled against the requirements of the CCM service interfaces as specified by ETSI GS NFV-IFA 036.

Sylva project plans to support ETSI NFV SOL018 the Kubernetes® API and ETSI NFV SOL020 the Kubernetes® Cluster API.

# Validation of CNFs with SYLVA

The cloud native infrastructure stack to be released by Sylva can be deployed to implement a validation platform aiming to verify that CNFs can be deployed on the stack, making successfully use of the capabilities integrated in the stack (e.g.: DPDK, SR-IOV, ...).

In the same way, using validated CNFs, it can be verified that any distribution created as a derived stack out of the Open Source released also incorporates the capabilities expected by Telco use cases.

# Contact

You can contact the Sylva team by [joining the dedicated slack channel](https://join.slack.com/t/sylva-projects/shared_invite/zt-1owzbom3p-MiSitCf~zJa8bFNoRjHKGQ)
